function techOpsPrint(limit, steps, i = 0) {

    if (i == limit) {
        return;
    }

    if (steps == 0) {
        console.error('Entrada inválida');
        return;
    }

    if (i % 3 == 0 && i % 5 != 0) {
        console.log('Tech');
    } else if (i % 5 == 0 && i % 3 != 0) {
        console.log('Ops');
    } else if (i % 5 == 0 && i % 3 == 0) {
        console.log('TechOps');
    } else {
        console.log(i);
    }

    return techOpsPrint(limit, steps, i += steps);
}