for (let i = 1; i <= 100; i++) {
    if (i % 3 == 0 && i % 5 != 0) {
        console.log('Tech');
    } else if (i % 5 == 0 && i % 3 != 0) {
        console.log('Ops');
    } else if (i % 5 == 0 && i % 3 == 0) {
        console.log('TechOps');
    } else {
        console.log(i);
    }

}