function techOpsPrint(limit, steps) {
    if (steps == 0) {
        console.error('Entrada inválida');
        return;
    }
    for (let i = 0; i <= limit; i += steps) {
        if (i % 3 == 0 && i % 5 != 0) {
            console.log('Tech');
        } else if (i % 5 == 0 && i % 3 != 0) {
            console.log('Ops');
        } else if (i % 5 == 0 && i % 3 == 0) {
            console.log('TechOps');
        } else {
            console.log(i);
        }
    }
}