function techOpsPrint(start, end, steps) {
    if (start < end) {
        for (let i = start; i <= end; i += steps) {
            if (i % 3 == 0 && i % 5 != 0) {
                console.log('Tech');
            } else if (i % 5 == 0 && i % 3 != 0) {
                console.log('Ops');
            } else if (i % 5 == 0 && i % 3 == 0 && i != 0) {
                console.log('TechOps');
            } else {
                console.log(i);
            }
        }
    } else {
        for (let i = start; i >= end; i -= steps) {
            if (i % 3 == 0 && i % 5 != 0) {
                console.log('Tech');
            } else if (i % 5 == 0 && i % 3 != 0) {
                console.log('Ops');
            } else if (i % 5 == 0 && i % 3 == 0 && i != 0) {
                console.log('TechOps');
            } else {
                console.log(i);
            }
        }
    }
}